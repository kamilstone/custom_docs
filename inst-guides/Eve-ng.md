# Instalation Guide of Eve-ng

To get more info please visit offical page of [Emulated Virtual Environment - Next generation](http://www.eve-ng.net/).

## Guide for VM on Windows 10
* If You already installed fresh Windows 10, there will be some problem:
  > Windows 10 disabled VT-x on oS via _Device Guard_ as a _Credential Guard and Virtualization Based Security_.

  To enable this option You should:

  1. Enable _Virtualization_ and _VT-x_ on system BIOS.
  2. Disable _Device Guard_

		1. Search in Windows Settings (There waa somewhere in "new-gui settings") or,
		2. Do this:

			* Download PS [script](https://www.microsoft.com/en-us/download/details.aspx?id=53337)
			* Try to execute `DG_Readiness_Tool_vx.x.ps1 -Disable `, but probably there will be also _Executing Policy Security_ disalowing You to execute unsigned PowerShell script.
			* If Your script are locked try to unlock this. Possibilites are documentated on [MSN docs page](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy?view=powershell-6).
			* **_Example 7_** was working for me first time. Quotation:
				```powershell
				The first command uses the **Set-ExecutionPolicy** cmdlet to change the execution policy to RemoteSigned.
				PS C:\> Set-ExecutionPolicy -ExecutionPolicy RemoteSigned

				The second command uses the Get-ExecutionPolicy cmdlet to get the effective execution policy in the session. The output shows that it is RemoteSigned.
				PS C:\> Get-ExecutionPolicy
				RemoteSigned

				The third command shows what happens when you run a blocked script in a Windows PowerShell session in which the execution policy is RemoteSigned. The RemoteSigned policy prevents you from running scripts that are downloaded from the Internet unless they are digitally signed.
				PS C:\> .\Start-ActivityTracker.ps1
				.\Start-ActivityTracker.ps1 : File .\Start-ActivityTracker.ps1 cannot be loaded. The file .\Start-ActivityTracker.ps1 
				is not digitally signed. The script will not execute on the system. For more information, see about_Execution_Policies 
				at http://go.microsoft.com/fwlink/?LinkID=135170. 
				At line:1 char:1
				+ .\Start-ActivityTracker.ps1
				+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~
				+ CategoryInfo          : NotSpecified: (:) [], PSSecurityException
				+ FullyQualifiedErrorId : UnauthorizedAccess

				The fourth command uses the Unblock-File cmdlet to unblock the script so it can run in the session.Before running an **Unblock-File** command, read the script contents and verify that it is safe.
				PS C:\> Unblock-File -Path "Start-ActivityTracker.ps1"

				The fifth and sixth commands show the effect of the **Unblock-File** command. The **Unblock-File** command does not change the execution policy. However, it unblocks the script so it will run in Windows PowerShell.
				PS C:\> Get-ExecutionPolicy
				RemoteSigned
				PS C:\> Start-ActivityTracker.ps1
				Task 1:
				```
			* After execution, PS output shows some erros, than it didn't find some _Registry keys_, but finally log  **_succes disabling DG/CG_** and _**enabling VT-x**_!
			* To update changes, You have to reboot Your OS. After than, it works.

* Now You can download in this case **_*.ova_** VM's file from [Eve servers](http://www.eve-ng.net/index.php/downloads/eve-ng).
* Run in VM emulator like [VirtualBox 5.2.8](https://www.virtualbox.org/wiki/Downloads) with [extension pack](https://download.virtualbox.org/virtualbox/5.2.8/Oracle_VM_VirtualBox_Extension_Pack-5.2.8-121009.vbox-extpack). But remember that this VM images was created with VHD dynamically-space-loaded to 40 GB.


## Configuration of Eve-ng
* First step is to login successfully to system. By reading [FAQ](http://www.eve-ng.net/index.php/faq) You can find that default accout has as following:
	> CLI   - root/eve
	>
	> WEB - admin/eve

* etc.