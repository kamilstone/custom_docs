# Custom docs 
### by _**Boar Artist**_.

This is only documentation repository. The title _Custom docs_ means there are documentation files written for fast reproduced guides for problems which I had in my software adventage.

## Proceeding
Remember that it is repository with submodules. If You already clone this without submodules, please update repository by:
```git
git submodule init
git submodule update
```

If you didn't You can do all off this via one command:
```git
git clone --recursive https://bitbucket.org/kamilstone/custom_docs.git
```

_Remember that this docs-repository content not have to working on Your station too, but could help with some best practises._

2018, Boar Artist